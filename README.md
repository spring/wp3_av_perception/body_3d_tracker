# 3d_body_tracker

## topics used
Node [/body_3d_tracker]
Publications: 
 * /humans/bodies/tracked [hri_msgs/IdsList]
 * /humans/candidate_matches [hri_msgs/IdsMatch]
 * /rosout [rosgraph_msgs/Log]
 * /tf [tf2_msgs/TFMessage]
 * /tracked_persons_2d [spring_msgs/TrackedPersons2d]

Subscriptions: 
 * /tf [tf2_msgs/TFMessage]
 * /tf_static [tf2_msgs/TFMessage]
 * /tracked_pose_2d/frame [spring_msgs/Frame]

Services: 
 * /body_3d_tracker/get_loggers
 * /body_3d_tracker/set_logger_level

## configuration files

The config directory contains :
* an intrinsinc calibration of the fisheye camera (projections/fisheye_calibration.yaml)
* an 'extrinsic'-like calibration of the fisheye camera as an homography matrix between the camera image and the floor. It can be regenerated using https://gitlab.inria.fr/spring/wp3_av_perception/omnicam/-/blob/master/demo/calibrate.py , and by placing four markers at given floor position with respect to the robot, and giving their pixel coordinates. 
