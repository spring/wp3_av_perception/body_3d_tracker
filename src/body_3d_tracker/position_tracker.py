from itertools import chain, count
from scipy.sparse.extract import find
import sklearn
from sklearn.neighbors import NearestNeighbors
import numpy as np
import time
from collections import namedtuple
from copy import deepcopy


TrackedPosition = namedtuple('TrackedPosition', ['vec', 'visual_id'])


def check_vec(v):
    if v is None:
        return False
    try:
        sklearn.utils.assert_all_finite(v)
    except:
        return False
    return True

class PositionTracker:
    def __init__(self, max_idx=20, dist_angle=0.3, memory_time=1, visual_id_coef=1.0, dist_threshold=0.5):
        self.last_positions = {}
        self.last_seen = {}
        self.approximate_position = {}
        self.last_idx = 0
        self.last_time = time.time()
        self.max_idx = max_idx
        self.dist_angle = dist_angle
        self.memory_time = memory_time
        self.visual_id_coef = visual_id_coef
        self.dist_threshold = dist_threshold  # 1.5 former version

    def vec(self, pt_list):
        f = lambda x,y,angle,visual_id : [x, y, x + self.dist_angle * np.cos(angle), y + self.dist_angle * np.sin(angle), self.visual_id_coef * visual_id]
        return np.array([f(*x.vec, x.visual_id) for x in pt_list])

    # get a new observation new_pts ({visual_id:position} dict) and try to associate it with previous records (self.last_positions)
    # - (a) if we find a previous record with the same visual_id, then we update it's position
    # - if a new observation is not in the previous record :
    #   - (b) if it's very close to an old observation, that has not been update in the previous case, then the visual_id might have changed
    #     so we change the visual_id of the record to the new one and update it's postion
    #   - (c) if we cannot find an old observation close enough and not already updated, then it's a new 'person' and a new record has to be
    #     created
    # - (d) once all new observations have been dealt with, if some old records still haven't been udated, remove them after a given time
    #      (self.memory_time)
    def update(self, new_obs):
        cur_time = time.time()
        new_pending_ids = list(new_obs.keys()) # list of new observations which have not been dealt with yet
        old_pending_ids = list(self.last_positions.keys()) # list of old records which have not been dealt with yet

        # (a) case
        for id in list(new_pending_ids):
            if id in self.last_positions:
                if check_vec(new_obs[id]):
                    # sometime the observed position is (nan,nan,nan)
                    # (when the person's skeletton is partially observed)
                    # in that cas we mark the person as seen, but we don't update its position
                    self.last_positions[id] = new_obs[id]
                    if np.linalg.norm(new_obs[id][:2]-self.last_positions[id][:2]) > self.dist_threshold:
                        rospy.logwarn("SUSPICIOUS ASSOCIATION !")
                new_pending_ids.remove(id)
                old_pending_ids.remove(id)
                self.last_seen[id] = cur_time

        for id in new_pending_ids:
            if check_vec(new_obs[id]):
                # if unmatched observation, but with corrupted position
                # we will just discard it
                for prev_id,prev_pos in self.last_positions.items():
                    if  np.linalg.norm(new_obs[id][:2]-prev_pos[:2]) < self.dist_threshold:
                        # (b) case
                        old_pending_ids.remove(prev_id)
                        self.last_positions.pop(prev_id)
                        break
                # (b) case continuated and (c) case
                self.last_positions[id] = new_obs[id]
                self.last_seen[id] = cur_time
            new_pending_ids.remove(id)

        # (d) case
        for id in old_pending_ids:
            if (cur_time - self.last_seen[id]) > self.memory_time:
                self.last_positions.pop(id)

    # UNUSED
    def push_prev(self, new_pts):
        dt = time.time() - self.last_time
        self.last_time = time.time()
        self.last_idx = self.max_idx
        updated_ids = []
        for pt in new_pts:
            try:
                j = next(j for j in self.last_positions.keys() if pt.visual_id == self.last_positions[j].visual_id)
                if check_vec(pt.vec):
                    self.last_positions[j] = pt
                    self.last_seen[j] = 0.0
                    self.approximate_position[j] = False
                    updated_ids.append(j)
                else:
                    self.last_seen[j] = 0.0
                    self.approximate_position[j] = True
                    updated_ids.append(j)
            except StopIteration:

                if check_vec(pt.vec):
                    for key, val in list(self.last_positions.items()):
                        dist = np.linalg.norm(pt.vec[:2]-val.vec[:2])
                        # if new with different visual_id from last_postions is too close from an last_postions[k] => this new is the last_positions[k] with new visual_id
                        if dist <= self.dist_threshold:
                            del self.last_positions[key]
                            del self.approximate_position[key]
                            del self.last_seen[key] 
                    # self.last_positions[self.last_idx] = pt
                    self.last_positions[pt.visual_id] = pt
                    self.last_seen[pt.visual_id] = 0
                    self.approximate_position[pt.visual_id] = False
                    updated_ids.append(pt.visual_id)
                    # self.last_idx += 1

        # self.fix_idx()
        not_updated_ids = set(self.last_positions.keys()) - set(updated_ids)

        for k in list(self.last_positions):
            # check if not_updated_ids is too close from an last_postions[k] => this not_updated_ids is the last_positions[k] with different visual_id
            for not_updated_id in not_updated_ids:
                if k != not_updated_id:
                    dist = np.linalg.norm(self.last_positions[not_updated_id].vec[:2]-self.last_positions[k].vec[:2])
                    if dist <= self.dist_threshold:
                        del self.last_positions[k]
                        del self.approximate_position[k]
                        del self.last_seen[k] 
            if k not in updated_ids:
                self.last_seen[k] += dt
                if (self.last_seen[k] > self.memory_time):
                    del self.last_positions[k]
                    del self.approximate_position[k]
                    del self.last_seen[k]


    def push_old(self, new_pts):
        dt = time.time() - self.last_time
        self.last_time = time.time()
        """Add the keypoints from a new frame into the buffer."""
        if not self.last_positions:  # First pass
            if len(new_pts) > self.max_idx:
                raise ValueError("Too many objects to track")
            i = 0
            for pt in new_pts:
                if pt.vec is not None:
                    self.last_positions[i] = pt
                    self.last_seen[i] = 0
                    self.approximate_position[i] = False
                    i += 1
                return       

        # Previously observed skeletons which aren't close to a new one:
        unseen = set(self.last_positions.keys())

        # We append new points starting after max_idx, they are then placed
        # in [0, self.max_idx) in function self.fix_idx()
        self.last_idx = self.max_idx

       

        # indices which have location data are matched to nearest neighbors
        """
        
        notnone_idx = [i for i in range(len(new_pts)) if check_vec(new_pts[i].vec)]
        if len(notnone_idx) > 0:
            match_keys = [k for k in self.approximate_position.keys() if self.approximate_position[k] == False]
            
            prev = self.vec([self.last_positions[k] for k in match_keys])
            curr = self.vec([new_pts[i] for i in notnone_idx])

            if len(match_keys) > 0:
                nn_model = NearestNeighbors(n_neighbors=1, algorithm='brute')
                nn_model.fit(prev)
                dists, neighbors = nn_model.kneighbors(curr, return_distance=True)
                neighbors = neighbors.flatten()
            else:
                dists = 2*self.dist_threshold*np.ones(len(notnone_idx))

            for i, id in enumerate(notnone_idx):
                if 0. < dists[i] <= self.dist_threshold:
                    neighbor = match_keys[neighbors[i]]
                    self.last_positions[neighbor] = new_pts[id]
                    self.last_seen[neighbor] = 0
                    self.approximate_position[neighbor] = False
                    unseen.discard(neighbor)
                else:
                    self.last_positions[self.last_idx] = new_pts[id]
                    self.last_idx += 1
        """
        """

        # indices which have no location data are matched to same visual id (and keep same location)
        none_idx = list(set(range(len(new_pts))))# - set(notnone_idx))
        to_discard = []
        if len(none_idx) > 0:
            unused_vidx = [new_pts[i].visual_id for i in none_idx]
            for i in unseen:
                try:
                    j = next(j for j in unused_vidx if self.last_positions[i].visual_id == j)
                    to_discard.append(i)
                    if new_pts[j].vec is not None:
                    self.approximate_position[i] = True
                except StopIteration:
                    continue
        for i in to_discard:
            unseen.discard(i)

        self.fix_idx()

        # Remove points which have not been matched using either location OR visual id
        # and have not been seen for self.memory_time s
        for i in unseen:
            self.last_seen[i] += dt
            if (self.last_seen[i] > self.memory_time):
                del self.last_positions[i]
        """

    def tracked_positions(self):
        return deepcopy(self.last_positions)

    def fix_idx(self):
        if len(self.last_positions) >= self.max_idx:
            raise ValueError("Length of tracks greater than the maximum allowed")
        else:
            j = 0
            for i in range(self.max_idx, self.last_idx):
                while j in self.last_positions:
                    j += 1
                self.last_positions[j] = self.last_positions.pop(i)
                self.last_seen[j] = 0.
                self.approximate_position[j] = False
