#!/usr/bin/env python3
import rospy
import numpy as np
from spring_msgs.msg import Frame, TrackedPerson2d, TrackedPersons2d
from hri_msgs.msg import IdsList, IdsMatch
from tf2_msgs.msg import TFMessage
from sensor_msgs.msg import RegionOfInterest, JointState
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3, PoseStamped
import tf
import omnicam
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from body_3d_tracker.utils import constraint_angle, ema_filter
from body_3d_tracker.position_tracker import PositionTracker, TrackedPosition
from copy import deepcopy
from scipy.interpolate import griddata

class Tracker3DBody:
    def __init__(self):
        self.robot_frame = rospy.get_param('~robot_frame', 'base_footprint')
        self.map_frame = rospy.get_param('~map_frame', 'map')
        self.frame_topic = rospy.get_param('~frame_topic', '/tracked_pose_2d/frame')
        self.openpose = rospy.get_param('~openpose', True)
        self.confidence_threshold = rospy.get_param('~confidence_threshold', 0.5)
        self.max_distance = rospy.get_param('~max_distance', 5)  # discard points further from x m
        self.max_idx = rospy.get_param('~max_idx', 20)
        self.memory_time = rospy.get_param('~memory_time', 5)
        self.dist_threshold = rospy.get_param('~dist_threshold', 0.5)
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)
        
        self.frame_data = None
        self.ids = []
        self.joint_state_hri_pub = dict()

        self.idx_l_heel = None
        self.idx_r_heel = None
        self.idx_l_big_toe = None
        self.idx_r_big_toe = None

        self.homo_mat = rospy.get_param('homo_mat', None)
        if self.homo_mat is None:
            rospy.logerr('homo_mat is None, homo_mat has not be loaded')
        self.homo_mat = np.array([self.homo_mat]).squeeze()
        self.geom = omnicam.OmniCameraGeometry(2.79,-0.09,0.5,-0.003,-0.002,1729.,1729.,637.,482.,1280,960)

        self.position_tracker = PositionTracker(
            max_idx=self.max_idx,
            memory_time=self.memory_time,
            dist_threshold=self.dist_threshold
        )
        # filtering coefficients for position and angle
        # between 0 and 1, value of 1 means no filtering
        # values near 0 means strong filtering
        self.alpha_angle = rospy.get_param('~alpha_angle_filter', 0.3)
        self.alpha_pos = rospy.get_param('~alpha_pos_filter', 0.3)
        self.cur_prev_dist_threshold = rospy.get_param('~cur_prev_dist_threshold', 0.2)
    
        self.previous_tracked_persons = {}
        self.current_tracked_persons = {}
        self.current_time = None

        self._publishers = []
        self._subscribers = []
        self._timers = []
        
        self.image_points = np.array([
[564, 956],
[571, 916],
[575, 884],
[579, 855],
[583, 830],
[586, 808],
[589, 790],
[592, 776],
[595, 761],
[597, 750],
[491, 948],
[505, 910],
[517, 879],
[526, 851],
[535, 827],
[542, 805],
[548, 789],
[554, 774],
[560, 761],
[564, 749],
[425, 931],
[445, 898],
[461, 870],
[475, 845],
[488, 822],
[499, 803],
[508, 786],
[517, 772],
[525, 759],
[531, 747],
[339, 940],
[364, 910],
[388, 883],
[408, 858],
[425, 835],
[442, 814],
[457, 797],
[470, 781],
[480, 767],
[490, 755],
[501, 744],
[527, 712],
[290, 916],
[317, 891],
[343, 865],
[366, 846],
[386, 825],
[406, 807],
[422, 790],
[436, 777],
[449, 764],
[461, 752],
[471, 741],
[250, 890],
[279, 871],
[307, 850],
[329, 833],
[350, 815],
[369, 798],
[387, 782],
[403, 771],
[418, 759],
[431, 748],
[444, 738],
[215, 864],
[242, 848],
[270, 831],
[292, 817],
[315, 802],
[336, 786],
[356, 774],
[373, 764],
[388, 753],
[403, 743],
[415, 735],
[186, 839],
[212, 826],
[239, 814],
[261, 802],
[285, 788],
[306, 775],
[327, 766],
[343, 756],
[360, 745],
[377, 737],
[391, 730],
[436, 705],
[165, 820],
[191, 811],
[216, 799],
[239, 788],
[260, 779],
[283, 767],
[302, 757],
[320, 749],
[337, 741],
[354, 733],
[368, 724],
[145, 800],
[170, 792],
[195, 784],
[217, 775],
[238, 767],
[259, 757],
[280, 749],
[298, 742],
[314, 734],
[331, 727],
[346, 719],
[132, 783],
[154, 777],
[177, 770],
[197, 763],
[219, 756],
[238, 749],
[259, 740],
[276, 734],
[292, 728],
[309, 722],
[326, 715],
[119, 768],
[140, 763],
[162, 757],
[180, 751],
[201, 746],
[220, 739],
[239, 732],
[257, 727],
[273, 721],
[289, 716],
[306, 711],
[358, 693],
[397, 678],
[111, 754],
[131, 749],
[148, 745],
[164, 740],
[185, 734],
[204, 729],
[224, 725],
[242, 721],
[258, 715],
[274, 711],
[290, 705],
[101, 740],
[123, 738],
[136, 734],
[153, 731],
[171, 725],
[190, 722],
[210, 717],
[225, 713],
[240, 708],
[256, 704],
[274, 699],
[91, 730],
[110, 726],
[127, 724],
[143, 720],
[160, 718],
[177, 714],
[197, 709],
[214, 707],
[227, 703],
[240, 698],
[252, 693],
[118, 716],
[619, 732],
[622, 722],
[623, 713],
[625, 707],
[588, 730],
[592, 722],
[595, 714],
[598, 707],
[558, 728],
[563, 720],
[568, 713],
[573, 705],
[527, 727],
[535, 719],
[540, 711],
[500, 724],
[508, 716],
[517, 708],
[523, 702],
[474, 722],
[483, 714],
[491, 708],
[499, 701],
[447, 719],
[457, 711],
[467, 704],
[477, 699],
[421, 715],
[433, 709],
[443, 702],
[400, 712],
[412, 705],
[424, 699],
[435, 693],
[378, 707],
[390, 702],
[402, 697],
[413, 691],
[357, 703],
[371, 698],
[381, 693],
[393, 688],
[339, 698],
[350, 693],
[363, 690],
[451, 659],
[323, 695],
[335, 691],
[349, 686],
[359, 681],
[308, 690],
[319, 685],
[331, 682],
[345, 678],
[293, 684],
[302, 681],
[311, 678],
[324, 674],
[566, 682],
[487, 678],
[360, 662],
[450, 659],
[392, 653],
[421, 644],
[716, 956],
[709, 916],
[705, 884],
[701, 855],
[697, 830],
[694, 808],
[691, 790],
[688, 776],
[685, 761],
[683, 750],
[789, 948],
[775, 910],
[763, 879],
[754, 851],
[745, 827],
[738, 805],
[732, 789],
[726, 774],
[720, 761],
[716, 749],
[855, 931],
[835, 898],
[819, 870],
[805, 845],
[792, 822],
[781, 803],
[772, 786],
[763, 772],
[755, 759],
[749, 747],
[941, 940],
[916, 910],
[892, 883],
[872, 858],
[855, 835],
[838, 814],
[823, 797],
[810, 781],
[800, 767],
[790, 755],
[779, 744],
[753, 712],
[990, 916],
[963, 891],
[937, 865],
[914, 846],
[894, 825],
[874, 807],
[858, 790],
[844, 777],
[831, 764],
[819, 752],
[809, 741],
[1030, 890],
[1001, 871],
[973, 850],
[951, 833],
[930, 815],
[911, 798],
[893, 782],
[877, 771],
[862, 759],
[849, 748],
[836, 738],
[1065, 864],
[1038, 848],
[1010, 831],
[988, 817],
[965, 802],
[944, 786],
[924, 774],
[907, 764],
[892, 753],
[877, 743],
[865, 735],
[1094, 839],
[1068, 826],
[1041, 814],
[1019, 802],
[995, 788],
[974, 775],
[953, 766],
[937, 756],
[920, 745],
[903, 737],
[889, 730],
[844, 705],
[1115, 820],
[1089, 811],
[1064, 799],
[1041, 788],
[1020, 779],
[997, 767],
[978, 757],
[960, 749],
[943, 741],
[926, 733],
[912, 724],
[1135, 800],
[1110, 792],
[1085, 784],
[1063, 775],
[1042, 767],
[1021, 757],
[1000, 749],
[982, 742],
[966, 734],
[949, 727],
[934, 719],
[1148, 783],
[1126, 777],
[1103, 770],
[1083, 763],
[1061, 756],
[1042, 749],
[1021, 740],
[1004, 734],
[988, 728],
[971, 722],
[954, 715],
[1161, 768],
[1140, 763],
[1118, 757],
[1100, 751],
[1079, 746],
[1060, 739],
[1041, 732],
[1023, 727],
[1007, 721],
[991, 716],
[974, 711],
[922, 693],
[883, 678],
[1169, 754],
[1149, 749],
[1132, 745],
[1116, 740],
[1095, 734],
[1076, 729],
[1056, 725],
[1038, 721],
[1022, 715],
[1006, 711],
[990, 705],
[1179, 740],
[1157, 738],
[1144, 734],
[1127, 731],
[1109, 725],
[1090, 722],
[1070, 717],
[1055, 713],
[1040, 708],
[1024, 704],
[1006, 699],
[1189, 730],
[1170, 726],
[1153, 724],
[1137, 720],
[1120, 718],
[1103, 714],
[1083, 709],
[1066, 707],
[1053, 703],
[1040, 698],
[1028, 693],
[1162, 716],
[661, 732],
[658, 722],
[657, 713],
[655, 707],
[692, 730],
[688, 722],
[685, 714],
[682, 707],
[722, 728],
[717, 720],
[712, 713],
[707, 705],
[753, 727],
[745, 719],
[740, 711],
[780, 724],
[772, 716],
[763, 708],
[757, 702],
[806, 722],
[797, 714],
[789, 708],
[781, 701],
[833, 719],
[823, 711],
[813, 704],
[803, 699],
[859, 715],
[847, 709],
[837, 702],
[880, 712],
[868, 705],
[856, 699],
[845, 693],
[902, 707],
[890, 702],
[878, 697],
[867, 691],
[923, 703],
[909, 698],
[899, 693],
[887, 688],
[941, 698],
[930, 693],
[917, 690],
[829, 659],
[957, 695],
[945, 691],
[931, 686],
[921, 681],
[972, 690],
[961, 685],
[949, 682],
[935, 678],
[987, 684],
[978, 681],
[969, 678],
[956, 674],
[714, 682],
[793, 678],
[920, 662],
[830, 659],
[888, 653],
[859, 644],
[639,957],
[640,917],
[637,884],
[635,855],
[634,829],
[631,808],
[631,790],
[630,774],
[631,760],
[631,749]
])
        self.room_points = np.array([
[0.75, 0.25],
[1.0, 0.25],
[1.25, 0.25],
[1.5, 0.25],
[1.75, 0.25],
[2.0, 0.25],
[2.25, 0.25],
[2.5, 0.25],
[2.75, 0.25],
[3.0, 0.25],
[0.75, 0.5],
[1.0, 0.5],
[1.25, 0.5],
[1.5, 0.5],
[1.75, 0.5],
[2.0, 0.5],
[2.25, 0.5],
[2.5, 0.5],
[2.75, 0.5],
[3.0, 0.5],
[0.75, 0.75],
[1.0, 0.75],
[1.25, 0.75],
[1.5, 0.75],
[1.75, 0.75],
[2.0, 0.75],
[2.25, 0.75],
[2.5, 0.75],
[2.75, 0.75],
[3.0, 0.75],
[0.5, 1.0],
[0.75, 1.0],
[1.0, 1.0],
[1.25, 1.0],
[1.5, 1.0],
[1.75, 1.0],
[2.0, 1.0],
[2.25, 1.0],
[2.5, 1.0],
[2.75, 1.0],
[3.0, 1.0],
[4.0, 1.0],
[0.5, 1.25],
[0.75, 1.25],
[1.0, 1.25],
[1.25, 1.25],
[1.5, 1.25],
[1.75, 1.25],
[2.0, 1.25],
[2.25, 1.25],
[2.5, 1.25],
[2.75, 1.25],
[3.0, 1.25],
[0.5, 1.5],
[0.75, 1.5],
[1.0, 1.5],
[1.25, 1.5],
[1.5, 1.5],
[1.75, 1.5],
[2.0, 1.5],
[2.25, 1.5],
[2.5, 1.5],
[2.75, 1.5],
[3.0, 1.5],
[0.5, 1.75],
[0.75, 1.75],
[1.0, 1.75],
[1.25, 1.75],
[1.5, 1.75],
[1.75, 1.75],
[2.0, 1.75],
[2.25, 1.75],
[2.5, 1.75],
[2.75, 1.75],
[3.0, 1.75],
[0.5, 2.0],
[0.75, 2.0],
[1.0, 2.0],
[1.25, 2.0],
[1.5, 2.0],
[1.75, 2.0],
[2.0, 2.0],
[2.25, 2.0],
[2.5, 2.0],
[2.75, 2.0],
[3.0, 2.0],
[4.0, 2.0],
[0.5, 2.25],
[0.75, 2.25],
[1.0, 2.25],
[1.25, 2.25],
[1.5, 2.25],
[1.75, 2.25],
[2.0, 2.25],
[2.25, 2.25],
[2.5, 2.25],
[2.75, 2.25],
[3.0, 2.25],
[0.5, 2.5],
[0.75, 2.5],
[1.0, 2.5],
[1.25, 2.5],
[1.5, 2.5],
[1.75, 2.5],
[2.0, 2.5],
[2.25, 2.5],
[2.5, 2.5],
[2.75, 2.5],
[3.0, 2.5],
[0.5, 2.75],
[0.75, 2.75],
[1.0, 2.75],
[1.25, 2.75],
[1.5, 2.75],
[1.75, 2.75],
[2.0, 2.75],
[2.25, 2.75],
[2.5, 2.75],
[2.75, 2.75],
[3.0, 2.75],
[0.5, 3.0],
[0.75, 3.0],
[1.0, 3.0],
[1.25, 3.0],
[1.5, 3.0],
[1.75, 3.0],
[2.0, 3.0],
[2.25, 3.0],
[2.5, 3.0],
[2.75, 3.0],
[3.0, 3.0],
[4.0, 3.0],
[5.0, 3.0],
[0.5, 3.25],
[0.75, 3.25],
[1.0, 3.25],
[1.25, 3.25],
[1.5, 3.25],
[1.75, 3.25],
[2.0, 3.25],
[2.25, 3.25],
[2.5, 3.25],
[2.75, 3.25],
[3.0, 3.25],
[0.5, 3.5],
[0.75, 3.5],
[1.0, 3.5],
[1.25, 3.5],
[1.5, 3.5],
[1.75, 3.5],
[2.0, 3.5],
[2.25, 3.5],
[2.5, 3.5],
[2.75, 3.5],
[3.0, 3.5],
[0.5, 3.75],
[0.75, 3.75],
[1.0, 3.75],
[1.25, 3.75],
[1.5, 3.75],
[1.75, 3.75],
[2.0, 3.75],
[2.25, 3.75],
[2.5, 3.75],
[2.75, 3.75],
[3.0, 3.75],
[1.0, 4.0],
[3.25, 0.25],
[3.5, 0.25],
[3.75, 0.25],
[4.0, 0.25],
[3.25, 0.5],
[3.5, 0.5],
[3.75, 0.5],
[4.0, 0.5],
[3.25, 0.75],
[3.5, 0.75],
[3.75, 0.75],
[4.0, 0.75],
[3.25, 1.0],
[3.5, 1.0],
[3.75, 1.0],
[3.25, 1.25],
[3.5, 1.25],
[3.75, 1.25],
[4.0, 1.25],
[3.25, 1.5],
[3.5, 1.5],
[3.75, 1.5],
[4.0, 1.5],
[3.25, 1.75],
[3.5, 1.75],
[3.75, 1.75],
[4.0, 1.75],
[3.25, 2.0],
[3.5, 2.0],
[3.75, 2.0],
[3.25, 2.25],
[3.5, 2.25],
[3.75, 2.25],
[4.0, 2.25],
[3.25, 2.5],
[3.5, 2.5],
[3.75, 2.5],
[4.0, 2.5],
[3.25, 2.75],
[3.5, 2.75],
[3.75, 2.75],
[4.0, 2.75],
[3.25, 3.0],
[3.5, 3.0],
[3.75, 3.0],
[6.0, 3.0],
[3.25, 3.25],
[3.5, 3.25],
[3.75, 3.25],
[4.0, 3.25],
[3.25, 3.5],
[3.5, 3.5],
[3.75, 3.5],
[4.0, 3.5],
[3.25, 3.75],
[3.5, 3.75],
[3.75, 3.75],
[4.0, 3.75],
[5.0, 1.0],
[5.0, 2.0],
[5.0, 4.0],
[6.0, 3.0],
[6.0, 4.0],
[7.0, 4.0],
[0.75, -0.25],
[1.0, -0.25],
[1.25, -0.25],
[1.5, -0.25],
[1.75, -0.25],
[2.0, -0.25],
[2.25, -0.25],
[2.5, -0.25],
[2.75, -0.25],
[3.0, -0.25],
[0.75, -0.5],
[1.0, -0.5],
[1.25, -0.5],
[1.5, -0.5],
[1.75, -0.5],
[2.0, -0.5],
[2.25, -0.5],
[2.5, -0.5],
[2.75, -0.5],
[3.0, -0.5],
[0.75, -0.75],
[1.0, -0.75],
[1.25, -0.75],
[1.5, -0.75],
[1.75, -0.75],
[2.0, -0.75],
[2.25, -0.75],
[2.5, -0.75],
[2.75, -0.75],
[3.0, -0.75],
[0.5, -1.0],
[0.75, -1.0],
[1.0, -1.0],
[1.25, -1.0],
[1.5, -1.0],
[1.75, -1.0],
[2.0, -1.0],
[2.25, -1.0],
[2.5, -1.0],
[2.75, -1.0],
[3.0, -1.0],
[4.0, -1.0],
[0.5, -1.25],
[0.75, -1.25],
[1.0, -1.25],
[1.25, -1.25],
[1.5, -1.25],
[1.75, -1.25],
[2.0, -1.25],
[2.25, -1.25],
[2.5, -1.25],
[2.75, -1.25],
[3.0, -1.25],
[0.5, -1.5],
[0.75, -1.5],
[1.0, -1.5],
[1.25, -1.5],
[1.5, -1.5],
[1.75, -1.5],
[2.0, -1.5],
[2.25, -1.5],
[2.5, -1.5],
[2.75, -1.5],
[3.0, -1.5],
[0.5, -1.75],
[0.75, -1.75],
[1.0, -1.75],
[1.25, -1.75],
[1.5, -1.75],
[1.75, -1.75],
[2.0, -1.75],
[2.25, -1.75],
[2.5, -1.75],
[2.75, -1.75],
[3.0, -1.75],
[0.5, -2.0],
[0.75, -2.0],
[1.0, -2.0],
[1.25, -2.0],
[1.5, -2.0],
[1.75, -2.0],
[2.0, -2.0],
[2.25, -2.0],
[2.5, -2.0],
[2.75, -2.0],
[3.0, -2.0],
[4.0, -2.0],
[0.5, -2.25],
[0.75, -2.25],
[1.0, -2.25],
[1.25, -2.25],
[1.5, -2.25],
[1.75, -2.25],
[2.0, -2.25],
[2.25, -2.25],
[2.5, -2.25],
[2.75, -2.25],
[3.0, -2.25],
[0.5, -2.5],
[0.75, -2.5],
[1.0, -2.5],
[1.25, -2.5],
[1.5, -2.5],
[1.75, -2.5],
[2.0, -2.5],
[2.25, -2.5],
[2.5, -2.5],
[2.75, -2.5],
[3.0, -2.5],
[0.5, -2.75],
[0.75, -2.75],
[1.0, -2.75],
[1.25, -2.75],
[1.5, -2.75],
[1.75, -2.75],
[2.0, -2.75],
[2.25, -2.75],
[2.5, -2.75],
[2.75, -2.75],
[3.0, -2.75],
[0.5, -3.0],
[0.75, -3.0],
[1.0, -3.0],
[1.25, -3.0],
[1.5, -3.0],
[1.75, -3.0],
[2.0, -3.0],
[2.25, -3.0],
[2.5, -3.0],
[2.75, -3.0],
[3.0, -3.0],
[4.0, -3.0],
[5.0, -3.0],
[0.5, -3.25],
[0.75, -3.25],
[1.0, -3.25],
[1.25, -3.25],
[1.5, -3.25],
[1.75, -3.25],
[2.0, -3.25],
[2.25, -3.25],
[2.5, -3.25],
[2.75, -3.25],
[3.0, -3.25],
[0.5, -3.5],
[0.75, -3.5],
[1.0, -3.5],
[1.25, -3.5],
[1.5, -3.5],
[1.75, -3.5],
[2.0, -3.5],
[2.25, -3.5],
[2.5, -3.5],
[2.75, -3.5],
[3.0, -3.5],
[0.5, -3.75],
[0.75, -3.75],
[1.0, -3.75],
[1.25, -3.75],
[1.5, -3.75],
[1.75, -3.75],
[2.0, -3.75],
[2.25, -3.75],
[2.5, -3.75],
[2.75, -3.75],
[3.0, -3.75],
[1.0, -4.0],
[3.25, -0.25],
[3.5, -0.25],
[3.75, -0.25],
[4.0, -0.25],
[3.25, -0.5],
[3.5, -0.5],
[3.75, -0.5],
[4.0, -0.5],
[3.25, -0.75],
[3.5, -0.75],
[3.75, -0.75],
[4.0, -0.75],
[3.25, -1.0],
[3.5, -1.0],
[3.75, -1.0],
[3.25, -1.25],
[3.5, -1.25],
[3.75, -1.25],
[4.0, -1.25],
[3.25, -1.5],
[3.5, -1.5],
[3.75, -1.5],
[4.0, -1.5],
[3.25, -1.75],
[3.5, -1.75],
[3.75, -1.75],
[4.0, -1.75],
[3.25, -2.0],
[3.5, -2.0],
[3.75, -2.0],
[3.25, -2.25],
[3.5, -2.25],
[3.75, -2.25],
[4.0, -2.25],
[3.25, -2.5],
[3.5, -2.5],
[3.75, -2.5],
[4.0, -2.5],
[3.25, -2.75],
[3.5, -2.75],
[3.75, -2.75],
[4.0, -2.75],
[3.25, -3.0],
[3.5, -3.0],
[3.75, -3.0],
[6.0, -3.0],
[3.25, -3.25],
[3.5, -3.25],
[3.75, -3.25],
[4.0, -3.25],
[3.25, -3.5],
[3.5, -3.5],
[3.75, -3.5],
[4.0, -3.5],
[3.25, -3.75],
[3.5, -3.75],
[3.75, -3.75],
[4.0, -3.75],
[5.0, -1.0],
[5.0, -2.0],
[5.0, -4.0],
[6.0, -3.0],
[6.0, -4.0],
[7.0, -4.0],
[0.75,0.0],
[1.0,0.0],
[1.25,0.0],
[1.5,0.0],
[1.75,0.0],
[2.0,0.0],
[2.25,0.0],
[2.5,0.0],
[2.75,0.0],
[3.0,0.0]
])
        self.switch_mapping_method = 1  # 0 for old method, 1 for new method that interpolates from the dictionary made from mapping image pixel points to floor points
        self._init()        
    
    def _init(self):
        if self.openpose:
            self.idx_l_heel = 21  # 21/29 ## openpose/mediapipe
            self.idx_r_heel = 24  # 24/30  ## openpose/mediapipe
            self.idx_l_big_toe = 19  # 19/31  ## openpose/mediapipe
            self.idx_r_big_toe = 22  # 22/32  ## openpose/mediapipe
        else:
            self.idx_l_heel = 29  # 21/29 ## openpose/mediapipe
            self.idx_r_heel = 30  # 24/30  ## openpose/mediapipe
            self.idx_l_big_toe = 31  # 19/31  ## openpose/mediapipe
            self.idx_r_big_toe = 32  # 22/32  ## openpose/mediapipe

        self._check_all_sensors_ready()
        
        self._subscribers.append(rospy.Subscriber(self.frame_topic, Frame, callback=self._frame_callback, queue_size=1))
        self._tracked_persons_2d_pub  = rospy.Publisher('/tracked_persons_2d', TrackedPersons2d, queue_size=10)
        self._ids_list_pub = rospy.Publisher('/humans/bodies/tracked', IdsList, queue_size=10, latch=True)
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
        self._publishers.append(self._tracked_persons_2d_pub)
        self._publishers.append(self._ids_list_pub)
        self._publishers.append(self.joint_state_hri_pub)        
        self._publishers.append(self._diagnostics_pub)

        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))
        
        self.tf_listener = tf.TransformListener()
        self.tf_broadcaster = tf.TransformBroadcaster()

        rospy.on_shutdown(self.shutdown)                

        rospy.loginfo("Track Frames Initialization Ended")


    def body_parts_to_local_position_and_orientation(self, body_parts):
        """
        Computes position and orientation in the local map, given body parts in the image plane.
        Returns : x, y, theta (theta=0 means person parallel to the robot)
        """
        if not body_parts:
            return None

        body_indices = [self.idx_l_heel, self.idx_r_heel, self.idx_l_big_toe, self.idx_r_big_toe]
        if np.any([body_parts[i].score < self.confidence_threshold for i in body_indices]):
            return None

        x_l_heel, y_l_heel =  body_parts[self.idx_l_heel].pixel.x, body_parts[self.idx_l_heel].pixel.y
        x_r_heel, y_r_heel =  body_parts[self.idx_r_heel].pixel.x, body_parts[self.idx_r_heel].pixel.y
        x_l_big_toe, y_l_big_toe =  body_parts[self.idx_l_big_toe].pixel.x, body_parts[self.idx_l_big_toe].pixel.y
        x_r_big_toe, y_r_big_toe =  body_parts[self.idx_r_big_toe].pixel.x, body_parts[self.idx_r_big_toe].pixel.y

        points_image_plane = np.array([[x_l_heel, y_l_heel],[x_r_heel, y_r_heel], [x_l_big_toe, y_l_big_toe], [x_r_big_toe, y_r_big_toe]])
        points_local_map = np.zeros((4, 2))

        if self.switch_mapping_method == 0: # old method
            for idx, point_2d in enumerate(points_image_plane):
                x, y, z = self.geom.lift_projective(point_2d[0], point_2d[1])
                x, y, z = np.dot(self.homo_mat, [x,y,z])
                points_local_map[idx, :] = [y/z, -x/z]
            center_point = np.mean(points_local_map, axis=0)
        else:   # new method
            points_local_map = self.interpolate_with_dict(points_image_plane)
            center_point = np.mean(points_local_map, axis=0)

        if np.linalg.norm(center_point) > self.max_distance:
            rospy.loginfo('self.max_distance {}'.format(np.linalg.norm(center_point)))
            return None
        angle = np.arctan2(((points_local_map[2, 1] - points_local_map[0, 1]) + (points_local_map[3, 1]-points_local_map[1, 1])),
                            (points_local_map[2, 0] - points_local_map[0, 0]) + (points_local_map[3, 0]-points_local_map[1, 0]))
        return center_point[0], center_point[1], angle
    
    def interpolate_with_dict(self,points): # new method that uses scipy interpolate and the dictionary made from mapping image pixel points to floor points
        points_local_map1 = np.zeros((4, 2))
        for idx, point_2d in enumerate(points):
            interpolate_x = griddata(self.image_points, self.room_points[:, 0], point_2d, method='cubic')
            interpolate_y = griddata(self.image_points, self.room_points[:, 1], point_2d, method='cubic')
            if np.isnan(interpolate_x):
                x, y, z = self.geom.lift_projective(point_2d[0], point_2d[1])
                x, y, z = np.dot(self.homo_mat, [x,y,z])
                print("Out of boundary - Using old method \n")
                interpolate_x = [y/z]
                interpolate_y = [-x/z]
            if np.isnan(interpolate_y):
                x, y, z = self.geom.lift_projective(point_2d[0], point_2d[1])
                x, y, z = np.dot(self.homo_mat, [x,y,z])
                interpolate_x = [y/z]
                interpolate_y = [-x/z]
            points_local_map1[idx, :] = [interpolate_x[0], interpolate_y[0]]
        return points_local_map1

    def local_to_global_map(self, x, y, angle):
        """
        Given x,y,angle in local robot map,
        computes x,y,angle in global map (self.map_frame) frame.
        Returns: x,y,angle
        """
        quat = tf.transformations.quaternion_from_euler(0, 0, angle)
        try:
            map_to_robot_tf = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, self.current_time)
        except:
            try:
                map_to_robot_tf = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, rospy.Time(0))
                rospy.logwarn("Could not get transformation between {} and {} at correct time. Using latest available.".format(self.map_frame, self.robot_frame))
            except:
                rospy.logerr("Could not get transformation between {} and {}.".format(self.map_frame, self.robot_frame))
                return x, y, angle

        map_to_robot_tf_mat = self.tf_listener.fromTranslationRotation(map_to_robot_tf[0], map_to_robot_tf[1])
        robot_to_human_tf_mat = self.tf_listener.fromTranslationRotation([x, y, 0.], quat)
        map_to_human_tf_mat = np.dot(map_to_robot_tf_mat, robot_to_human_tf_mat)

        map_to_human_tvec = tf.transformations.translation_from_matrix(map_to_human_tf_mat)
        cos_angle, sin_angle = map_to_human_tf_mat[0, 0], map_to_human_tf_mat[1, 0]
        angle = np.arctan2(sin_angle, cos_angle)
        x_c, y_c = map_to_human_tvec[0], map_to_human_tvec[1]
        return x_c, y_c, angle

    def get_id_list(self):
        self.current_time = self.frame_data.header.stamp
        new_pts = {}

        for person_idx, person in enumerate(self.frame_data.persons):
            p_local = self.body_parts_to_local_position_and_orientation(person.bodyParts)
            if p_local is None:
                # new_pts.append(TrackedPosition(vec=None, visual_id=self.frame_data.ids[person_idx]))
                continue
            p_global = self.local_to_global_map(*p_local)
            if p_global is None:
                # new_pts.append(TrackedPosition(vec=None, visual_id=self.frame_data.ids[person_idx]))
                continue
            x,y,angle = p_global
            new_pts[self.frame_data.ids[person_idx]]=np.array([x, y, angle])
        rospy.loginfo('new_pts {}'.format(new_pts))
        # send the new poses to the tracker and update its state
        self.position_tracker.update(new_pts)

    # filter noise on pose estimates
    # old/new_tracked_persons is a dict whose keys are persons ids, 
    # and values are previous/current persons poses

    def filter_poses(self):
        #if it's the first call, then old_poses empty, return unfiltered poses
        if self.previous_tracked_persons:
            for id, cur_person in self.current_tracked_persons.items():
                #filter pose only if this person already present in the previous frames
                if id in self.previous_tracked_persons.keys() :
                    prev_person = self.previous_tracked_persons[id]
                    
                    cur_x,cur_y,cur_theta = cur_person[:]
                    prev_x,prev_y,prev_theta = prev_person[:]
                    if (cur_x - prev_x > self.cur_prev_dist_threshold) or (cur_y - prev_y > self.cur_prev_dist_threshold) : 
                        rospy.logwarn("ERROR BETWEEN PERSON POSES WAY TOO BIG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    else: 
                        new_x = ema_filter(cur_x, prev_x, self.alpha_pos)
                        new_y = ema_filter(cur_y, prev_y, self.alpha_pos)
                        # in order to avoid the angle discontinuity at (-pi,+pi)
                        # we projet theta on the unit disk, filter this postion, and get back the angle
                        new_ptheta_x=ema_filter(np.cos(cur_theta),np.cos(prev_theta),self.alpha_angle)
                        new_ptheta_y=ema_filter(np.sin(cur_theta),np.sin(prev_theta),self.alpha_angle)
                        new_theta = np.arctan2(new_ptheta_y, new_ptheta_x)
                        self.current_tracked_persons[id] = np.array([new_x,new_y,new_theta])
 
        self.previous_tracked_persons = deepcopy(self.current_tracked_persons)
        
    def _publish_tracked_persons_2d(self):
        tracked_persons_2d = TrackedPersons2d()
        ids = []

        for id, pose in self.current_tracked_persons.items():
            angle = pose[2]
            quat = tf.transformations.quaternion_from_euler(0, 0, angle)
            x, y  = pose[:2]
            visible = 0

            map_to_human_mat = self.tf_listener.fromTranslationRotation([x, y, 0.], quat)
            trans_map_2_human = tf.transformations.translation_from_matrix(map_to_human_mat)
            quat_map_2_human = tf.transformations.quaternion_from_matrix(map_to_human_mat)

            self.tf_broadcaster.sendTransform(
             trans_map_2_human,
             quat_map_2_human,
             self.current_time,
             "body_b{0:0=4d}".format(id),
             self.map_frame
            )

            lin_vel = 0.
            ang_vel = 0.

            rospy.logdebug("id : {}, pos : {}, vel : {}".format(id, pose[:2], pose[-2:]))

            tracked_person_2d = TrackedPerson2d()
            tracked_person_2d.track_id = "b{0:0=4d}".format(id)
            tracked_person_2d.velocity3D = Twist(Vector3(lin_vel, 0, 0), Vector3(0, 0, ang_vel))
            tracked_person_2d.bounding_box = RegionOfInterest(0, 0, 0, 0, False)
            tracked_person_2d.confidence = float(visible)
            tracked_person_2d.pose3D = PoseStamped()
            tracked_person_2d.pose3D.pose = Pose(Point(x, y, 0.), Quaternion(*quat))
            tracked_person_2d.pose3D.header.stamp = self.current_time

            tracked_persons_2d.detections.append(tracked_person_2d)

            topic_name = "b{0:0=4d}".format(id)
            if topic_name not in self.joint_state_hri_pub.keys():
                self.joint_state_hri_pub[topic_name] = rospy.Publisher('/humans/bodies/' + topic_name + '/join_states', JointState, queue_size=10)
            joint_state_hri = JointState()
            joint_state_hri.header = tracked_person_2d.pose3D.header
            self.joint_state_hri_pub[topic_name].publish(joint_state_hri)
            ids.append(topic_name)

        tracked_persons_2d.header.stamp = self.current_time
        self._tracked_persons_2d_pub.publish(tracked_persons_2d)

        id_msg = IdsList()
        id_msg.header.stamp = self.current_time
        id_msg.ids = ids
        self._ids_list_pub.publish(id_msg)
        for id in list(self.joint_state_hri_pub.keys()):
            if id not in ids:
                self.joint_state_hri_pub[id].unregister()
                del self.joint_state_hri_pub[id]
        self.ids = list(ids)


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_frame_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_frame_ready(self):
        self.frame_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.frame_topic))
        while self.frame_data is None and not rospy.is_shutdown():
            try:
                self.frame_data = rospy.wait_for_message(self.frame_topic, Frame, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.frame_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting 2D openpose frames".format(self.frame_topic))
        return self.frame_data

    # Main callback
    def _frame_callback(self, data):
        self.frame_data = data
        # given new skeletton/body_id observations,
        # get 2d top down pose estimations
        # and track them (associate them to the previous persons)
        self.get_id_list()
        # get the tracker results
        self.current_tracked_persons = self.position_tracker.tracked_positions()
        #  and filter the poses ()
        self.filter_poses()
        self._publish_tracked_persons_2d()


    def shutdown(self):
        rospy.loginfo("Stopping the tracker_3d_body node")
        self.close()
        rospy.loginfo("Killing the tracker_3d_body node")


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: AV Perception: 3D Body Tracker'
        status.values.append(KeyValue(key='Number of detected skeletons with Openpose', value='{}'.format(len(self.frame_data.persons))))
        status.values.append(KeyValue(key='Number of 3D body joint state published', value='{}'.format(len(self.joint_state_hri_pub))))
        status.values.append(KeyValue(key='Number of 3D body tracked', value='{}'.format(len(self.ids))))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if not self.frame_data:
            status.level = DiagnosticStatus.OK
            status.message = 'No skeleton detected with Openpose in the front fisheye 2D plane as input'
        if self.frame_data.persons and not self.ids:
            status.level = DiagnosticStatus.WARN
            status.message = 'Was not able to track in 3D any detected skeletons from Opensepose'

        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()
