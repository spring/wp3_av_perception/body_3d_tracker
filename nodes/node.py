#!/usr/bin/env python3
import rospy

from body_3d_tracker.body_3d_tracker_node import Tracker3DBody


if __name__ == '__main__':
    # Init node
    rospy.init_node('tracker_3d_body', log_level=rospy.DEBUG, anonymous=True)
    node = Tracker3DBody()
    # node.run()
    rospy.spin()
   
